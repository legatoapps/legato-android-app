package com.example.myapplication;

import com.example.myapplication.tarsos.PitchDetectionResult;

public interface TunerUpdate {
    void updateNote(Note newNote, PitchDetectionResult result);
}