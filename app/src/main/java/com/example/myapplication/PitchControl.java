package com.example.myapplication;

public interface PitchControl {
    void play(double frequency);
    void play(Note note);
    void stop();
}
