package com.example.myapplication;
import android.animation.Animator;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class TunerActivity extends AppCompatActivity {
    private static final String TAG = TunerActivity.class.getSimpleName();
    private TunerFragment tunerFragment;
    private PitchFragment pitchFragment;
    private boolean showCancel;
    private Button btnStart;
    private MediaPlayer mPlayer;
    private Button btnPause;
    private Button btnRefresh;
    private boolean btnIsClick;
    private RelativeLayout rLay;
    private TextView txtText;
    private int i=0;
    public  String[] Text;
    private Handler handler;
    private Runnable task;
    private static int[] mas = {10000,14000,15000,20000,14000,19000,13000,15000,17000,20000,16000,34000,13000,39000};

    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showCancel = false;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        setContentView(R.layout.tuner_activity);
        btnIsClick = false;
        mPlayer = MediaPlayer.create(this, R.raw.titanicmyheartwillgoon);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stop();
            }
        });
        btnStart = (Button) findViewById(R.id.btnStart);
        btnPause = (Button) findViewById(R.id.btnPause);
        btnRefresh = (Button) findViewById(R.id.btnRefresh);
        rLay = (RelativeLayout)findViewById(R.id.rLay);
        txtText = (TextView)findViewById(R.id.txtText);
        Resources res = getResources();
        Text = res.getStringArray(R.array.textSong);
        btnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (btnIsClick == false) {
                    tunerFragment = new TunerFragment();
                    btnIsClick = true;

                    getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, tunerFragment, TunerFragment.TAG).commit();
                    rLay.setVisibility(View.VISIBLE);
                    handler = new Handler();
                    task = new Runnable() {
                        @Override
                        public void run() {

                            setText(i++);
                            handler.postDelayed(this, mas[i-1]);
                        }
                    };
                    handler.postDelayed(task, 24000);
                }
                btnStart.setVisibility(View.GONE);
                mPlayer.start();
                if(btnIsClick == false)
                {
                    handler.postDelayed(task, 10000);
                }

            }
        });
        btnPause.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPlayer.pause();
                btnStart.setVisibility(View.VISIBLE);
                handler.removeCallbacks(task);
            }
        });
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPlayer.pause();
                mPlayer.seekTo(0);
                mPlayer.start();
                i=0;
                txtText.setText(Text[i]);
                handler = new Handler();
                task = new Runnable() {
                    @Override
                    public void run() {
                        setText(i++);
                        handler.postDelayed(this, 7000);
                    }
                };
                handler.postDelayed(task, 17000);

            }
        });
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void stop() {
    }

    private void setText(int i ){
        txtText.setText(Text[i]);
    }
    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        if (showCancel) {
            transitionBackToTunerFragment(pitchFragment.unreveal(pitchFragment.getX(), pitchFragment.getY()));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case TunerFragment.AUDIO_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (tunerFragment != null) {
                        tunerFragment.init();
                    }
                } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(TunerActivity.this, "GuitarTuner needs access to the microphone to function.", Toast.LENGTH_LONG).show();
                    TunerActivity.this.finish();
                }
                break;
        }
    }

    public void transitionToPitchFragment(Note note, float x, float y) {
        if (!showCancel) {
            pitchFragment = PitchFragment.newInstance(note, x, y);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, pitchFragment, PitchFragment.TAG).commit();
            tunerFragment.stop();
            showCancel = true;
            invalidateOptionsMenu();
        }
    }

    public void transitionBackToTunerFragment(Animator anim) {
        if (anim != null) {
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    getSupportFragmentManager().beginTransaction().remove(pitchFragment).commit();
                    getSupportFragmentManager().executePendingTransactions();
                    tunerFragment.start();
                    showCancel = false;
                    invalidateOptionsMenu();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            anim.start();
        } else {
            getSupportFragmentManager().beginTransaction().remove(pitchFragment).commit();
            tunerFragment.start();
            showCancel = false;
            invalidateOptionsMenu();
        }
    }

    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Tuner Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

}

